package com.jl.socialapp.fragments;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.ProfilePictureView;
import com.jl.socialapp.R;
import com.jl.socialapp.adapters.TweetAdapter;
import com.jl.socialapp.utils.ButteryProgressBar;
import com.jl.socialapp.utils.Device;

import java.util.List;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Created by JL on 12/20/2014.
 */
public class MainFragment extends Fragment {

    private ProfilePictureView profilePictureView;
    private TextView profileNameView;
    private SearchView searchView;
    private ListView tweetsListView;
    private TextView tweetsEmptyView;
    private TextView lastKeywordView;
    private ButteryProgressBar progressBar;

    private Twitter twitterClient;
    private boolean isTwitterClientReady;

    private UiLifecycleHelper uiHelper;
    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(final Session session, final SessionState state, final Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        uiHelper = new UiLifecycleHelper(getActivity(), callback);
        uiHelper.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main, container, false);
        tweetsListView = (ListView) root.findViewById(R.id.tweetsListView);
        tweetsEmptyView = (TextView) root.findViewById(R.id.tweetsEmptyView);
        progressBar = (ButteryProgressBar) root.findViewById(R.id.progressBar);

        // setup the profile name view
        profileNameView = (TextView) root.findViewById(R.id.selection_user_name);
        String name = getName();
        if (!name.isEmpty())
            profileNameView.setText("Hello,\n" + name);

        // setup the profile pic view
        profilePictureView = (ProfilePictureView) root.findViewById(R.id.selection_profile_pic);
        profilePictureView.setCropped(true);

        // setup the search view
        searchView = (SearchView) root.findViewById(R.id.searchView);
        searchView.clearFocus();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                // close the keyboard
                searchView.clearFocus();

                if (!Device.isConnected(getActivity())) {
                    showMessage("Internet connection is required");
                    return false;
                }

                saveLastKeyword(s);
                searchTwitter(s);
                lastKeywordView.setText(s);
                searchView.setQuery("", false);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        // setup last keyword view
        lastKeywordView = (TextView) root.findViewById(R.id.lastKeywordView);
        lastKeywordView.setText(getLastKeyword());

        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Check for an open session
        Session session = Session.getActiveSession();
        if (session != null && session.isOpened()) {
            // Get the user's data
            makeMeRequest(session);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        uiHelper.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    private void onSessionStateChange(final Session session, SessionState state, Exception exception) {
        if (session != null && session.isOpened()) {
            // Get the user's data.
            makeMeRequest(session);
        }
    }

    private void makeMeRequest(final Session session) {
        // Make an API call to get user data and define a
        // new callback to handle the response.
        Request request = Request.newMeRequest(session,
                new Request.GraphUserCallback() {
                    @Override
                    public void onCompleted(GraphUser user, Response response) {
                        // If the response is successful
                        if (session == Session.getActiveSession()) {
                            if (user != null) {
                                // Set the id for the ProfilePictureView
                                // view that in turn displays the profile picture.
                                profilePictureView.setProfileId(user.getId());
                                // Set the Textview's text to the user's name.
                                profileNameView.setText("Hello,\n" + user.getName());
                                // save to prefs
                                saveName(user.getName());
                            }
                        }
                        if (response.getError() != null) {
                            // Handle errors, will do so later.
                        }
                    }
                });
        request.executeAsync();
    }

    private void showMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    private void setUpTwitter4j() {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setUseSSL(true);
        cb.setApplicationOnlyAuthEnabled(true);
        cb.setOAuthConsumerKey(getString(R.string.key_twitter_api_key));
        cb.setOAuthConsumerSecret(getString(R.string.key_twitter_api_secret));

        TwitterFactory tf = new TwitterFactory(cb.build());
        twitterClient = tf.getInstance();
        try {
            twitterClient.getOAuth2Token();
            isTwitterClientReady = true;
        } catch (TwitterException e) {
            e.printStackTrace();
        }
    }

    private void searchTwitter(String keyword) {
        String hashTagged = "#" + keyword;
        new SearchTask().execute(hashTagged);
    }

    private void saveLastKeyword(String keyword) {
        SharedPreferences prefs = getActivity().getSharedPreferences("keyword", 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("last_key", keyword);
        editor.commit();
    }

    private String getLastKeyword() {
        SharedPreferences prefs = getActivity().getSharedPreferences("keyword", 0);
        return prefs.getString("last_key", "");
    }

    private void saveName(String name) {
        SharedPreferences prefs = getActivity().getSharedPreferences("keyword", 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("user_name", name);
        editor.commit();
    }

    private String getName() {
        SharedPreferences prefs = getActivity().getSharedPreferences("keyword", 0);
        return prefs.getString("user_name", "");
    }

    private void showLoading() {
        // TODO: disable searchView
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideLoading() {
        // TODO: enable searchView
        progressBar.setVisibility(View.GONE);
    }

    private class SearchTask extends AsyncTask<String, Void, List<Status>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();
        }

        @Override
        protected List<twitter4j.Status> doInBackground(String... strings) {

            if (!isTwitterClientReady)
                setUpTwitter4j();

            List<twitter4j.Status> statuses = null;

            String keyword = strings[0];
            Query query = new Query(keyword);
            query.setCount(5);

            try {
                QueryResult result = twitterClient.search(query);
                statuses = result.getTweets();
            } catch (TwitterException e) {
                e.printStackTrace();
            }

            return statuses;
        }

        @Override
        protected void onPostExecute(List<twitter4j.Status> statuses) {
            super.onPostExecute(statuses);

            TweetAdapter adapter = new TweetAdapter(getActivity(), statuses);
            tweetsListView.setAdapter(adapter);

            if (statuses.size() == 0) {
                if (tweetsListView.getVisibility() == View.VISIBLE) {
                    tweetsListView.setVisibility(View.GONE);
                    tweetsEmptyView.setVisibility(View.VISIBLE);
                }
            } else {
                if (tweetsListView.getVisibility() == View.GONE) {
                    tweetsListView.setVisibility(View.VISIBLE);
                    tweetsEmptyView.setVisibility(View.GONE);
                }
            }

            hideLoading();
        }
    }
}
