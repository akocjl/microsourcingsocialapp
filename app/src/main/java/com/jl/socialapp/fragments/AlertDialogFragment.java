package com.jl.socialapp.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

public class AlertDialogFragment extends DialogFragment {

    private static final String ARGS_TITLE = "title";
    private static final String ARGS_MESSAGE = "message";
    private static final String ARGS_POSITIVE = "positive";
    private static final String ARGS_NEGATIVE = "negative";
    private static final String ARGS_REQUEST_CODE = "request_code";

    private AlertDialogInterface mListener;

    public static DialogFragment newInstance(int messageId, int positiveId, int negativeId, int requestCode) {
        AlertDialogFragment frag = new AlertDialogFragment();
        Bundle args = new Bundle();
        args.putInt(ARGS_MESSAGE, messageId);
        args.putInt(ARGS_POSITIVE, positiveId);
        args.putInt(ARGS_NEGATIVE, negativeId);
        args.putInt(ARGS_REQUEST_CODE, requestCode);
        frag.setArguments(args);
        return  frag;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mListener = (AlertDialogInterface) activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int message = getArguments().getInt(ARGS_MESSAGE);
        int positive = getArguments().getInt(ARGS_POSITIVE);
        int negative = getArguments().getInt(ARGS_NEGATIVE);
        final int requestCode = getArguments().getInt(ARGS_REQUEST_CODE);

        return new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton(positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mListener.onPositive(requestCode);
                    }
                })
                .setNegativeButton(negative, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mListener.onNegative(requestCode);
                    }
                }).create();
    }

    public interface AlertDialogInterface {
        void onPositive(int requestCode);
        void onNegative(int requestCode);
    }
}
