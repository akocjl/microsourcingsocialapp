package com.jl.socialapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jl.socialapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import twitter4j.MediaEntity;
import twitter4j.Status;

/**
 * Created by JL on 12/21/2014.
 */
public class TweetAdapter extends ArrayAdapter<Status> {

    private Context mContext;

    public TweetAdapter(Context context, List<Status> objects) {
        super(context, R.layout.list_item_tweet, objects);
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            // inflate the layout
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.list_item_tweet, parent, false);

            holder.textView = (TextView) convertView.findViewById(R.id.textView);
            holder.usernameView = (TextView) convertView.findViewById(R.id.usernameView);
            holder.handleView = (TextView) convertView.findViewById(R.id.handleView);
            holder.userPicView = (ImageView) convertView.findViewById(R.id.userPicView);
            holder.extraPicView = (ImageView) convertView.findViewById(R.id.extraPicView);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Status tweet = getItem(position);
        holder.textView.setText(tweet.getText());
        holder.usernameView.setText(tweet.getUser().getName());
        holder.handleView.setText("@" + tweet.getUser().getScreenName());

        String picURL = tweet.getUser().getBiggerProfileImageURL();
        Picasso.with(mContext).load(picURL).into(holder.userPicView);

        MediaEntity entities[] = tweet.getMediaEntities();
        if (entities != null && entities.length > 0) {
            String extraPicURL = entities[0].getMediaURL();
            Picasso.with(mContext).load(extraPicURL).into(holder.extraPicView);
        } else {
            holder.extraPicView.setImageDrawable(null);
        }

        return convertView;
    }

    private class ViewHolder {
        ImageView userPicView;
        TextView usernameView;
        TextView handleView;
        TextView textView;
        ImageView extraPicView;
    }
}
